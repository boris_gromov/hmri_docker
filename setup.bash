#!/usr/bin/env

HOST_HOME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

unset iface iface_ip master_uri master_ip ip roslaunch_wait master_location

if [ ! -z "$1" ]; then
 iface=$1
 iface_ip=$(ip addr show ${iface} | grep "inet\b" | awk '{print $2}' | cut -d/ -f1)
 echo "Using ${iface} interface to set ROS_IP: ${iface_ip}"
fi

# Set defaults if environment is not set
master_uri=${ROS_MASTER_URI:-http://localhost:11311}
master_ip=$(echo ${master_uri} | awk -F/ '{print $3}' | awk -F: '{print $1}' | tr -d '[[:space:]]')
ip=${iface_ip:-${ROS_IP:-localhost}}

# Check if we use external master or not
# if so, docker_core_auto  will wait for external master to start
if [ ${master_ip} = ${ip} ]; then
  roslaunch_wait=""
  master_location="local"
else
  roslaunch_wait="--wait"
  master_location="external"
fi

echo -e "Environment to be used for docker containers:\n\tROS_MASTER_URI=$master_uri (${master_location})\n\tROS_IP=$ip"

V="-v /tmp/env:/env -v $HOST_HOME/settings/$HOSTNAME.env:/env/settings.env"
E="-e ROS_MASTER_URI=${master_uri} -e ROS_IP=${ip}"
CONFIG="--privileged --net=host $V $E"

alias run_ros='docker run -it --rm -v $HOST_HOME/catkin_ws/src:/home/root/catkin_ws/src:ro \
--net=host --device=/dev/fw1 --device=/dev/fw0 --device=/dev/$(readlink /dev/myrobot):/dev/myrobot   --device=/dev/input/js0 --device=/dev/$(readlink /dev/mylaser):/dev/mylaser \
jeguzzi/pioneer:dev'

alias prun_ros='docker run -it --rm -v /tmp/env:/env -v $HOST_HOME/catkin_ws/src:/home/root/catkin_ws/src:ro \
--privileged --net=host jeguzzi/pioneer:dev'

alias pioneer_core='docker run -it --rm $CONFIG jeguzzi/pioneer:dev roslaunch pioneer_outdoor $HOSTNAME.launch'



alias ros_master='docker run -d --restart=always --name roscore  $CONFIG jeguzzi/pioneer:dev roscore'

alias pioneer_core_auto='docker run -d --restart=always $CONFIG jeguzzi/pioneer:dev roslaunch pioneer_outdoor $HOSTNAME.launch ${roslaunch_wait}'

alias pioneer_localization='docker run -it --rm $CONFIG jeguzzi/pioneer:dev roslaunch pioneer_outdoor outdoor_localization.launch'

alias pioneer_i_localization='docker run -it --rm $CONFIG jeguzzi/pioneer:dev roslaunch pioneer_outdoor indoor_localization.launch'

alias pioneer_control='docker run -it --rm $CONFIG jeguzzi/pioneer:dev roslaunch pioneer_outdoor outdoor_navigation.launch sick:=1'


alias pioneer_network='docker run -it --rm $CONFIG jeguzzi/pioneer:dev roslaunch pioneer_outdoor network.launch nimbro:=1'



alias pioneer_it='docker run -it --rm $CONFIG jeguzzi/pioneer:dev'
alias pioneer_itm='docker run -it --rm $CONFIG -v /var/run/docker.sock:/var/run/docker.sock jeguzzi/pioneer:monitor'



alias pioneer_camera='docker run -d  --name camera  $CONFIG jeguzzi/pioneer:dev roslaunch pioneer_outdoor camera-core.launch quality:=15 --wait'
alias pioneer_laser='docker run -d  --name laser  $CONFIG jeguzzi/pioneer:dev roslaunch pioneer_outdoor laser.launch sick:=1 --wait'
alias pioneer_gps='docker run -d  --name gps  $CONFIG jeguzzi/pioneer:dev roslaunch pioneer_outdoor gps-core.launch --wait'

alias pioneer_lcm='docker run -d $CONFIG --name lcm jeguzzi/pioneer:dev roslaunch pioneer_outdoor lcm.launch'

alias pioneer_core_core='docker run -d --restart=always --name core  $CONFIG jeguzzi/pioneer:dev roslaunch pioneer_outdoor core-core.launch model:=$HOSTNAME'
alias pioneer_core_robot='docker run -d --restart=always --name p3at  $CONFIG jeguzzi/pioneer:dev roslaunch pioneer_outdoor core_robot.launch p2os:=1 --wait'
alias pioneer_monitor='docker run -d --restart=always --name ui $CONFIG -v /var/run/docker.sock:/var/run/docker.sock jeguzzi/pioneer:monitor roslaunch pioneer_outdoor monitor.launch --wait'

alias pioneer_video='docker run -d --restart=always --name video_stream $CONFIG -v /var/run/docker.sock:/var/run/docker.sock jeguzzi/pioneer:monitor roslaunch pioneer_outdoor video.launch --wait'

alias pioneer_ahrs='docker run -d --restart=always --name ahrs  $CONFIG jeguzzi/pioneer:dev roslaunch pioneer_outdoor ahrs-core.launch --wait'

alias pioneer_odom='docker run -d --restart=always --name odom $CONFIG jeguzzi/pioneer:dev roslaunch pioneer_outdoor odom_localization.launch --wait'



alias pioneer_control_indoor='docker run -d --restart=always --name control $CONFIG jeguzzi/pioneer:dev roslaunch pioneer_outdoor indoor_navigation.launch hokuyo:=1 global_static_map:=false global_frame:=odom --wait'

alias pioneer_leds='docker run -d --restart=always --name leds $CONFIG boris/hmri roslaunch hmri_leds leds_ns.launch startup_anim:=3 --wait'


alias ros='docker run -it --rm $CONFIG boris/hmri bash -c'
