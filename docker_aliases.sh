#!/bin/bash

# E.g. ~/.bash_aliases

## Docker Cleanup Commands
## Credits: https://www.calazan.com/docker-cleanup-commands/

# Kill all running containers
alias docker_kill='docker kill $(docker ps -q)'

# Stop all running containers
alias docker_stop='docker stop $(docker ps -q)'

# Delete all stopped containers
alias docker_clean='printf "\n>>> Deleting stopped containers\n\n" && docker rm $(docker ps -a -q)'

# Stop all running containers and delete them
alias docker_clean_stop='docker_stop || docker_clean'

# Delete all 'untagged/dangling' (<none>) images
# WARNING: also deletes data-only containers!
alias docker_clean_images='printf "\n>>> Deleting untagged images\n\n" && docker rmi $(docker images -q -f dangling=true)'

# Delete all stopped containers and untagged images.
alias docker_clean_all='docker_clean_containers || true && docker_clean_images'
